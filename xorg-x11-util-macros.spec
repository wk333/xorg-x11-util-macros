%global packagename util-macros
%global debug_package %{nil}

Name: 		xorg-x11-util-macros
Version: 	1.19.2
Release: 	4
License: 	MIT
URL: 		http://www.x.org
BuildArch: 	noarch
Summary: 	X.Org X11 macros
Source0:  	https://www.x.org/pub/individual/util/util-macros-%{version}.tar.bz2

Requires: autoconf automake libtool pkgconfig

%description
X.Org X11 autotools macros required for building the various packages that
comprise the X Window System.

%prep
%setup -q -n %{packagename}-%{version}

%build
%configure
%make_build

%install
%make_install

%files
%doc COPYING ChangeLog
%{_datadir}/aclocal/xorg-macros.m4
%{_datadir}/pkgconfig/xorg-macros.pc
%{_datadir}/util-macros

%changelog
* Wed Nov 27 2019 hexiaowen <hexiaowen@huawei.com> - 1.19.2-4
- Rebuilt

* Wed Nov 27 2019 hexiaowen <hexiaowen@huawei.com> - 1.19.2-3
- Rebuilt

* Tue Aug 27 2019 hexiaowen <hexiaowen@huawei.com> - 1.19.2-2
- Renew HTTPS URLs

* Tue Aug 27 2019 hexiaowen <hexiaowen@huawei.com> - 1.19.2-1
- util-macros 1.19.2
